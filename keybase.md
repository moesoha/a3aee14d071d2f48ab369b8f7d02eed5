### Keybase proof

I hereby claim:

  * I am moesoha on github.
  * I am soha (https://keybase.io/soha) on keybase.
  * I have a public key whose fingerprint is C0C8 8AEB 0CF8 BA40 2DA5  B831 27DB 133C D5EB D3BE

To claim this, I am signing this object:

```json
{
    &quot;body&quot;: {
        &quot;key&quot;: {
            &quot;eldest_kid&quot;: &quot;0101ee7f8ddb9f1115381b600148729586bdc3319acb1a062010c3e48ac1354c49000a&quot;,
            &quot;fingerprint&quot;: &quot;c0c88aeb0cf8ba402da5b83127db133cd5ebd3be&quot;,
            &quot;host&quot;: &quot;keybase.io&quot;,
            &quot;key_id&quot;: &quot;27db133cd5ebd3be&quot;,
            &quot;kid&quot;: &quot;0101ee7f8ddb9f1115381b600148729586bdc3319acb1a062010c3e48ac1354c49000a&quot;,
            &quot;uid&quot;: &quot;6ae6c5916d7c7d4cdbc2c344ace96819&quot;,
            &quot;username&quot;: &quot;soha&quot;
        },
        &quot;merkle_root&quot;: {
            &quot;ctime&quot;: 1500998090,
            &quot;hash_meta&quot;: &quot;cfd2b5efef964f1996c5ae2dbcb3513aee2b2cdb4107dd16098b9243ae7e06e6&quot;,
            &quot;seqno&quot;: 1279348
        },
        &quot;revoke&quot;: {
            &quot;sig_ids&quot;: [
                &quot;372867d3335aeb78b4139d791313a6ad9bdc08d5c9e4a9de30b5c71549b9800e0f&quot;
            ]
        },
        &quot;service&quot;: {
            &quot;name&quot;: &quot;github&quot;,
            &quot;username&quot;: &quot;moesoha&quot;
        },
        &quot;type&quot;: &quot;web_service_binding&quot;,
        &quot;version&quot;: 1
    },
    &quot;ctime&quot;: 1500998133,
    &quot;expire_in&quot;: 157680000,
    &quot;prev&quot;: &quot;9d24c7e5607c6fe5f27d3015b87e2e904fd3bfa29c5029447f6c2417b118621f&quot;,
    &quot;seqno&quot;: 27,
    &quot;tag&quot;: &quot;signature&quot;
}
```

with the key [C0C8 8AEB 0CF8 BA40 2DA5  B831 27DB 133C D5EB D3BE](https://keybase.io/soha), yielding the signature:

```
-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.0.73
Comment: https://keybase.io/crypto

yMKmAnicrVNfiFRVHJ6xLXNhUKMCt43k5pLg6p5/9557trU2aDMCK1SGyq3x/Pnd
mes4c8d77+y6LFuYSFS07UMPkiGsILlukIaSLLXKKCGshS+VqUREhFSsD7VvUZ27
KCH02NPhnPP9vu/7fed3zhfuyLXnrx15a+Rn9lwpP9cKm7kXh8OFUUdFZsTpHXWq
sLjALgNJWqqGxul1EEYYgAe+MUoEGGOX+lh5CGHmcyJc31NGU4qF1ApL5BFboCkw
X2pMXaaZQAhJp9sJwnoZ4kYc1lNLq5H2fQkK6cBXkiFipKt8igk3ClOqjQvKUAW2
sBIlWYU1p2QCG8LIntlNadHef+D/Z9/NRTpPgqddgT3DNTdMG6WJpoxJDcLzsciA
CcR1WQOLTqKKdMa6nRrE1V1QiqMozYLVaZhdYxchIXwkkG1OJpVSDVKZZRIYolwI
IBAeC7AQVlECsVKKuphKAKKIVWYYcWOwh4SvBGH2ggPywLMeEthdj6wC4YIy3zqI
YSiqQiaehGUbWeL0bncoJ77HDaXU8ivuW0YqDBeYWhVPGmGjQb5xtQAmhQGKlKs5
dplQ1jYCFDgvj2Vi8VCoF8lv9l0O00pT3Z5FLYJbcaQjjexkGFTpZm1JhXVjB8OW
DEGchFHdmrfI25OyD9ztwJ5GGEMpzBAu96wPZPNr2AYtpTCEaQ6uh7j2AnADOxcU
YTtSHAgIxAI7HIEkQruICMZ44GnCMFcY+x7Bwb/REW59ynL2iGG5LtNmDM5Y6+xg
Wy7fnrvrziXZj8m1L1tx6x9dWbn078FP1+e/U/z1r9cV82rjQwvD44WzYcjmnzl9
8amp4qaBpNx5ofboj6f2Ijlz+NXlT3R0NvnAWvnGwxdbN/Z9ef90JU53pIdfW7H1
p0fe9O7tu956Vs//9eTBiZ0zg9/3vjRx/NeVV6fGHsOj06Nk3+WRL+7+ZPtnfs+q
+ReubXn30MDJLa3THdz96Plz26Li6lzniYmjx94ZvzFzZveaj78p8P51Hxzsmloz
uefAXNR1bHJyyT1tHy4U+//4fW7/+P5L1WT6hwd+G79v+nrbwOTenj/P77zauLx5
6y9LTx09VFi++sErs48/3ToZ9X3+dqG/75XZZRfWvr8KNqzv6unYdGLzgcGvvn1v
Y3HW/QfQH3Xv
=5VR5
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/soha

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id soha
```
